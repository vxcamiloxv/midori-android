package org.midorinext.android.browser

import org.midorinext.android.preference.IntEnum

/**
 * The available proxy choices.
 */
enum class DrawerLineChoice(override val value: Int) : IntEnum {
    ONE(0),
    TWO(1),
    THREE(2)
}