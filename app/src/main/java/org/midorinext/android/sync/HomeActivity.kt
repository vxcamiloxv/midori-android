package org.midorinext.android.sync

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.parse.ParseUser
import org.midorinext.android.R

class HomeActivity : AppCompatActivity() {
    var tvName: TextView? = null
    var tvEmail: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_astian)
        val currentUser = ParseUser.getCurrentUser()
        tvName = findViewById(R.id.tvName)
        tvEmail = findViewById(R.id.tvEmail)
        if (currentUser != null) {
            /* tvName.setText(currentUser.getString("name")) */
            /* tvEmail.setText(currentUser.email)*/
        }
    }

    fun logout(view: View?) {
        val progress = ProgressDialog(this)
        progress.setMessage("Loading ...")
        progress.show()
        ParseUser.logOut()
        val intent = Intent(this@HomeActivity, LoginActivity::class.java)
        startActivity(intent)
        finish()
        progress.dismiss()
    }
}