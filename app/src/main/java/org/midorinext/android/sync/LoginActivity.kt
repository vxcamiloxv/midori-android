package org.midorinext.android.sync

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.parse.ParseUser
import org.midorinext.android.R

class LoginActivity : AppCompatActivity() {
    var edEmail: EditText? = null
    var edPassword: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_astian)
        edEmail = findViewById(R.id.edEmail)
        edPassword = findViewById(R.id.edPassword)
        if (ParseUser.getCurrentUser() != null) {
            val intent = Intent(this@LoginActivity, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    fun login(view: View?) {
        if (TextUtils.isEmpty(edEmail!!.text)) {
            edEmail!!.error = "Email is requited"
        } else if (TextUtils.isEmpty(edPassword!!.text)) {
            edPassword!!.error = "Password is required"
        } else {
            val progress = ProgressDialog(this)
            progress.setMessage("Loading ...")
            progress.show()
            ParseUser.logInInBackground(edEmail!!.text.toString(), edPassword!!.text.toString()) { user, e ->
                progress.dismiss()
                if (user != null) {
                    Toast.makeText(this@LoginActivity, R.string.welcome, Toast.LENGTH_LONG).show()
                    val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    ParseUser.logOut()
                    Toast.makeText(this@LoginActivity, e.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun signup(view: View?) {
        val intent = Intent(this@LoginActivity, SignupActivity::class.java)
        startActivity(intent)
    }

    fun forgotPassword(view: View?) {
        val intent = Intent(this@LoginActivity, ResetPasswordActivity::class.java)
        startActivity(intent)
    }
}